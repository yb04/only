package com.yb.multipartfile.controller;

import com.yb.multipartfile.util.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;

@RestController
public class FileUploadController {
    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
    //定义一个时间格式
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd/");

    FileUpload fileUpload=new FileUpload();

     @PostMapping("/upload")
     public String upload(@RequestParam("upload") MultipartFile upload) {
         // 获取文件的后缀名
          /*String suffixName = fileName.substring(fileName.lastIndexOf("."));
        logger.info("上传的后缀名为：" + suffixName);*/
         String filePath = "E://picture//";
          String s=fileUpload.upload(upload,filePath);
          if (!s.equals("上传失败")){
              System.out.println(s);
          }else {
              return "上传失败";
          }
       return  "上传成功";
     }
    @PostMapping("/uploads")
    public String uploads(@RequestParam("uploads") MultipartFile[] uploads) {
        for (MultipartFile upload : uploads) {
            String filePath = "E://picture//";
            String s=fileUpload.upload(upload,filePath);
            if (!s.equals("上传失败")){
                System.out.println(s);
            }else {
                return "上传失败";
            }

        }
        return  "上传成功";
    }

}
