package com.yb.multipartfile.controller;

import com.yb.multipartfile.dao.BookDao;
import com.yb.multipartfile.pojo.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {
      @Autowired
      RedisTemplate redisTemplate;
      @Autowired
      StringRedisTemplate stringRedisTemplate;
      @Autowired
      BookDao bookDao;
      @Autowired
      MongoTemplate mongoTemplate;
      @GetMapping("/test1")
      public  void  test1(){
/*          ValueOperations<String,String> opsl=stringRedisTemplate.opsForValue();
          opsl.set("name","三国演义");
          String name=opsl.get("name");
          System.out.println(name);
          ValueOperations ops2=redisTemplate.opsForValue();
          Book b1=new Book();
          b1.setId(1);
          b1.setName("红楼梦");
          b1.setAuthor("曹雪芹");
          ops2.set("b1",b1);
          Book book= (Book) ops2.get("b1");
          System.out.println(book.toString());*/

          List<Book> books=new ArrayList<>();
          Book b1=new Book();
          b1.setId(1);
          b1.setName("鲁迅");
          b1.setAuthor("朝花夕拾");
          //books.add(b1);

          Book b2=new Book();
          b2.setId(2);
          b2.setName("鲁迅");
          b2.setAuthor("呐喊");
         // books.add(b2);

         /* bookDao.insert(books);
          List<Book> books1=bookDao.findByAuthorContaining("鲁迅");
          System.out.println(books1.toString());
          Book book=bookDao.findByNameEquals("朝花夕拾");
          System.out.println(book.toString());*/

         List<Book> list=mongoTemplate.findAll(Book.class);
          System.out.println(list.toString());
          Book book=mongoTemplate.findById(1,Book.class);
          System.out.println(book.toString());


      }
}
