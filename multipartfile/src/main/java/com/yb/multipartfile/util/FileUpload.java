package com.yb.multipartfile.util;

import com.yb.multipartfile.controller.FileUploadController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class FileUpload {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
    //定义一个时间格式
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd/");
    @PostMapping("/upload")
    public String upload(@RequestParam("upload") MultipartFile upload,String filePath) {
        if (upload.isEmpty()) {
            return "文件为空";
        }
        // 获取文件名
        String fileName = upload.getOriginalFilename();
        logger.info("上传的文件名为：" + fileName);

        File dest = new File(filePath + fileName);
        // 检测是否存在目录
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            upload.transferTo(dest);
            return dest.getParentFile().getName();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "上传失败";
    }
}
