package com.yb.multipartfile.dao;

import com.yb.multipartfile.pojo.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BookDao extends MongoRepository<Book,Integer> {
    List<Book> findByAuthorContaining(String author);

    Book findByNameEquals(String name);

}
