package com.yb.authentication.mapper;

import com.yb.authentication.model.Role;
import com.yb.authentication.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    User loadUserByUsername(String username);
    List<Role> getuserRolesByUid(Integer id);
}
