package com.yb.restful07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Restful07Application {

    public static void main(String[] args) {
        SpringApplication.run(Restful07Application.class, args);
    }

}
