package com.yb.restful07.repository;

import com.yb.restful07.pojo.Book;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface BookRepository01 extends MongoRepository<Book,Integer> {
}
