package com.yb.cache;

import com.yb.cache.dao.BookDao;
import com.yb.cache.pojo.Book;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
class CacheApplicationTests {
    @Autowired
    BookDao bookDao;

    @Test
    void contextLoads() {
        bookDao.getBookById(1);
        bookDao.getBookById(1);
        bookDao.deleteBookById(1);
        Book b3=bookDao.getBookById(1);
        System.out.println("b3:"+b3);
        Book b=new Book();
        b.setName("三国演义");
        b.setAuthor("罗贯中");
        b.setId(1);
        bookDao.updateBookById(b);
        Book b4=bookDao.getBookById(1);
        System.out.println("b4:"+b4);
    }

}
